﻿using FactoryMethod.Proxy;

TextHandler textHandler = new TextHandler();
User user = new User(Rol.Manager, "username", "password");
IText proxy = new Proxy(textHandler, user);

proxy.write("Some text is being showed...");
proxy.write("loving design patterns");
Console.WriteLine(proxy.read());


User jrdev = new User(Rol.JrDev, "dev", "1234");
IText proxyJr = new Proxy(textHandler, jrdev);
proxyJr.write("   me too!");
Console.WriteLine(proxyJr.read());

// TextHandler textHandler = new TextHandler();
// User user = new User(Rol.Dev, "username", "password");
// IText proxy = new Proxy2(textHandler, user);
// proxy.write("Some text is being showed...");
// proxy.write("loving design patterns");
// Console.WriteLine(proxy.read());