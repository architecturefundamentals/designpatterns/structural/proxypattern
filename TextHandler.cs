namespace FactoryMethod.Proxy
{
    public class TextHandler : IText
    {
        private String text= "";

        public string read()
        {
            return this.text;
        }

        public void write(string text)
        {
           this.text = this.text + " " + text;
        }
    }
}