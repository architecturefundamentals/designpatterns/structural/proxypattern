namespace FactoryMethod.Proxy
{
    public class User
    {
        public Rol Rol{
            get;
            set;
        }
        public String Name{
            get;
            set;
        } 
        public String Password{
            get;
            set;
        }
        public User(Rol rol, String name, String password){
            this.Rol = rol;
            this.Name = name;
            this.Password = password;
        }

        
    }
}