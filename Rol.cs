namespace FactoryMethod.Proxy
{
    public enum Rol
    {
        Manager,
        Owner,
        Dev,
        JrDev,
        QA
    }
}