using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactoryMethod.Proxy
{
    public class Proxy2: AbstractProxy, IText
    {
        
        public Proxy2(TextHandler textHandler, User user): base(textHandler, user){
           
        }
        public override Boolean checkAccess(){
            if (this.user.Rol == Rol.Owner || this.user.Rol == Rol.Manager || this.user.Rol == Rol.Dev){
                return true;
            }else{
                return false;
            }
        }
    }
}