# PROXY METHOD PATTERN

---
## A bit of context ...
>### _What is this pattern about?_
>This pattern it's a structural pattern, it lets you provide a substitute or placeholder for another object. A proxy controls access to the original object, allowing you to perform something either before or after the request gets through to the original object.
>
> It's usually used in the next cases:
>  * Access control (protection proxy). This is when you want only specific clients to be able to use the service object.
>  * Lazy initialization (virtual proxy). This is when you have a heavyweight service object that wastes system resources by being always up, even though you only need it from time to time.
>  * Local execution of a remote service (remote proxy). This is when the service object is located on a remote server.
>  * Logging requests (logging proxy). This is when you want to keep a history of requests to the service object.
>  *  Caching request results (caching proxy). This is when you need to cache results of client requests and manage the life cycle of this cache, especially if results are quite large.
> 
---
## But this simple example is about...

![Demo](img/Adapter Pattern-Proxy.jpg)

and Done!
:smile:
---
_Tech: C#_ 